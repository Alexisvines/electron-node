const { app, BrowserWindow } = require('electron');
const {ipcMain} = require('electron');
const Store = require('./store.js');
const CronJob = require('cron').CronJob;
let shell = require('shelljs');


  // Mantén una referencia global del objeto window, si no lo haces, la ventana
  // se cerrará automáticamente cuando el objeto JavaScript sea eliminado por el recolector de basura.
  let win;
  let newWindowArray=[];
  let childLogArray=[];
  let windowIdArray=[];
  let comando;
  let ejecutarComando;


function openNewWindow(key) {

    let newWindow;

    if (newWindow) {
        newWindow.focus();
        return;
    }

    newWindow = new BrowserWindow({
        height: 1024,
        width: 960,
        title: key
    });

    newWindow.setTitle(key);
    newWindow.loadFile('log.html');

    newWindow.on('page-title-updated', (evt) => {
        evt.preventDefault();
    });

    newWindow.on('close',function(data){
        console.log(windowIdArray[newWindow.id]);
        childLogArray[windowIdArray[newWindow.id]].kill('SIGINT');
        delete newWindowArray[windowIdArray[newWindow.id]];
    });

    newWindow.on('closed', function (data) {
        newWindow = null
    });

    windowIdArray[newWindow.id] = key;
    newWindowArray[key]=newWindow;

}


let datos;
let busqueda;
let idCluster = '1';
let idPodRT = '';
let username;
let os = process.platform;
let grepMetodo= new Map();
grepMetodo.set("win32", "findstr");
grepMetodo.set("darwin", "grep");


const store = new Store({
    // We'll call our data file 'user-preferences'
    configName: 'user-data',
    defaults: {
        username: ""
    }
});

function createWindow () {
    // Crea la ventana del navegador.
    win = new BrowserWindow({ width: 800, height: 800, darkTheme: true,icon: __dirname + '/ginyu.ico'});

    // y carga el archivo index.html de la aplicación.
    win.loadFile('index.html');
    username = store.get('username');
    console.log(username);

    ipcMain.on('finish-loading', (event, arg) => {
        event.sender.send('saved-username', username);

    });
    extracted();

    // Emitido cuando la ventana es cerrada.
    win.on('closed', () => {

      // Elimina la referencia al objeto window, normalmente  guardarías las ventanas
      // en un vector si tu aplicación soporta múltiples ventanas, este es el momento
      // en el que deberías borrar el elemento correspondiente.
        //cmd.run('Taskkill /IM kubectl.exe /F');
      win = null;
      newWindowArray = null;
    })
  }
  app.on('ready', createWindow);

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  });
  
  app.on('activate', () => {
    if (win === null) {
      createWindow()
    }

  });


function extracted() {

    const job = new CronJob('0 */1 * * * *', function () {

        buscarPod();
        console.log('Cada minuto se actualiza la busqueda de pod : ' + busqueda);
    });
    console.log('After job instantiation');

    ipcMain.on('log-bluemix', (event, arg) => {

        datos = arg;
        var shell = require('shelljs');
        var child = shell.exec('bx login', {async: true});

        child.stdout.on(
            'data',
            function (data) {
                if (data.includes("Email")) {
                    child.stdin.write(datos.username + "\n");
                }
                if (data.includes("Password")) {
                    child.stdin.write(datos.password + "\n");
                }
                if (data.includes("Seleccione una cuenta")) {
                    child.stdin.write("1\n");
                }
                if (data.includes("Desea actualizar?")) {
                    child.stdin.write("N\n");
                }
                if (data.includes("ERROR")) {
                    event.sender.send('login-reply', 'errorLogin');
                }
            }
        );

        child.on("exit", function (code) {
            if (code === 0) {
                store.set('username', datos.username);
                console.log("Termino bluemix shell");
                shell.exec(
                    'bx cs clusters',
                    function (err, data, stderr) {
                        let arregloAmbientes = data.split("\n");
                        let arregloAmbientesFiltrado = [];
                        let flag = false;
                        for (let k = 0; k < arregloAmbientes.length; k++) {
                            if (flag) {
                                arregloAmbientesFiltrado.push(arregloAmbientes[k]);
                            }
                            if (arregloAmbientes[k].includes("Name")) {
                                flag = true;
                            }
                        }

                        if (arregloAmbientesFiltrado.length > 0) {
                            let jsonArregloAmbientes = [];
                            for (let i = 0; i < arregloAmbientesFiltrado.length; i++) {
                                let arregloAmbientes2 = arregloAmbientesFiltrado[i].replace(/  +/g, ' ').split(' ');
                                if (arregloAmbientes2[0] !== "" && arregloAmbientes2[1] !== "" && arregloAmbientes2[2] !== "") {
                                    let objetoAmbientes = {
                                        "nombre": arregloAmbientes2[0],
                                        "id": arregloAmbientes2[1],
                                        "estado": arregloAmbientes2[2]
                                    };
                                    jsonArregloAmbientes.push(objetoAmbientes);

                                }
                            }

                            event.sender.send('seleccione-ambiente', jsonArregloAmbientes.slice(0, -1));

                        }
                    }
                );
            }
        });
    });

    ipcMain.on('ambiente-seleccionado', (event, arg) => {
        idCluster = arg;
        shell.exec(
            'bx cs cluster-config' + ' ' + idCluster,
            function (err, data, stderr) {
                console.log('variable entorno inicio : ' + process.env.KUBECONFIG)
                let arregloData = data.split("\n");
                console.log("arregloData[3]" + arregloData[3]);
                let pathKube = arregloData[3].split("=");

                process.env.KUBECONFIG = pathKube[1];
                console.log('variable entorno final : ' + process.env.KUBECONFIG)
                event.sender.send('login-reply', 'buscarPod');
            }
        );
    });

    ipcMain.on('buscar-pod', (event, arg) => {
        console.log("OS : " + os);
        busqueda = arg;
        console.log(arg);
        buscarPod();
        job.start();

    });

    function buscarPod() {
        shell.exec(
            'kubectl -n bci-api get pods | ' + grepMetodo.get(os) + ' ' + busqueda,
            function (err, data, stderr) {
                console.log(data);
                console.log(err);
                console.log(stderr);
                let arregloPods = data.split("\n");
                let jsonArregloPods = [];
                for (let i = 0; i < arregloPods.length; i++) {
                    let arregloPods2 = arregloPods[i].replace(/  +/g, ' ').split(' ');
                    let myObj = {
                        "id": arregloPods2[0],
                        "cantidad": arregloPods2[1],
                        "estado": arregloPods2[2],
                        "fallbacks": arregloPods2[3],
                        "life-time": arregloPods2[4]
                    };
                    jsonArregloPods.push(myObj);
                }
                win.webContents.send('respuesta-busqueda', jsonArregloPods.slice(0, -1));

                //  event.sender.send('respuesta-busqueda', jsonArregloPods.slice(0, -1));
            }
        );
    }


    ipcMain.on('ejecutar-comando', (event, arg) => {
        if(comando!== undefined && comando!==arg && ejecutarComando!==null){
            ejecutarComando.kill();

        }
        if(arg!==null && arg!==undefined){
            comando = arg;
        }
        console.log("Se intenta ejecutar el siguiente comando : " + comando);
        ejecutarComando = shell.exec(comando,{async: true});
        if(ejecutarComando!==null) {
            ejecutarComando.stdout.on('data', function (data) {
                event.sender.send("linea", data);

            });
        }


    });

    ipcMain.on('bajar-log', (event, arg) => {
        console.log(arg);
        shell.exec('kubectl -n bci-api logs' + ' ' + arg + ' > ' + arg + '.txt',{async: true})

    });

    ipcMain.on('ver-log-rt', (event, arg) => {

        openNewWindow(arg);
        if (idPodRT !== '') {
            event.sender.send("borrar-log", "");
        }

        idPodRT = arg;
        childLogArray[arg]= shell.exec('kubectl -n bci-api logs --tail=50 -f ' + arg, {async: true});
        childLogArray[arg].stdout.on('data', function (data) {
            if(newWindowArray[arg]===undefined||newWindowArray[arg]===null){
                return;
            }
            newWindowArray[arg].webContents.send('log', data);
            event.sender.send("linea", data);
        });

    });

}
